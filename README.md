# BIMM143 Classwork

To view portfolio, please click [HERE](https://sean_wong.gitlab.io/bioinformatics)

## Contents
Class 5: [Data Visualization with R](https://sean_wong.gitlab.io/bioinformatics/05.html)

Class 6: [R functions](https://sean_wong.gitlab.io/bioinformatics/06.html)

Class 7: [Functions and Packages](https://sean_wong.gitlab.io/bioinformatics/07.html)

Class 8: [Unsupervised Learning I](https://sean_wong.gitlab.io/bioinformatics/08.html)

Class 9: [Unsupervised Learning II](https://sean_wong.gitlab.io/bioinformatics/09.html)

Class 11: [Structural Bioinformatics I](https://sean_wong.gitlab.io/bioinformatics/11.html)

Class 12: [Structural Bioinformatics II](https://sean_wong.gitlab.io/bioinformatics/12.html)

Class 13: [Genome Informatics I](https://sean_wong.gitlab.io/bioinformatics/13.html)

Class 14: [Genome Informatics II](https://sean_wong.gitlab.io/bioinformatics/14.html)

Class 15: [Pathway Analysis and the Interpretation of Gene Lists](https://sean_wong.gitlab.io/bioinformatics/15.html)

Class 17: [Biological Network Analysis](https://sean_wong.gitlab.io/bioinformatics/17.html)

Class 18: [Cancer genomics and Immunoinformatics](https://sean_wong.gitlab.io/bioinformatics/18.html)

Find A Gene Project: [Trifolium Repens](https://sean_wong.gitlab.io/bioinformatics/BIMM143_S19_scw025.pdf)